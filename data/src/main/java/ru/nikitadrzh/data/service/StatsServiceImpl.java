package ru.nikitadrzh.data.service;

import java.util.List;

import io.reactivex.Single;
import ru.nikitadrzh.data.model.ApiStats;

public class StatsServiceImpl implements StatsService {

    private StatsService statsService = RetrofitInitializer.getInstance().getStatsService();

    @Override
    public Single<List<ApiStats>> getStats() {
        return statsService.getStats();
    }
}

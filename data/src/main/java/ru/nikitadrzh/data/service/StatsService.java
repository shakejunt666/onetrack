package ru.nikitadrzh.data.service;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import ru.nikitadrzh.data.model.ApiStats;

public interface StatsService {

    @GET("metric.json")
    Single<List<ApiStats>> getStats();
}

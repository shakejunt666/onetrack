package ru.nikitadrzh.data.service;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.nikitadrzh.data.BuildConfig;

/**
 * Retrofit initializer
 */
public class RetrofitInitializer {
    private static RetrofitInitializer retrofitInstance = new RetrofitInitializer();
    private static StatsService statsService;

    private static final String BASE_URL = "https://intern-f6251.firebaseio.com/intern/";

    public static RetrofitInitializer getInstance() {
        return retrofitInstance;
    }

    private RetrofitInitializer() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel((BuildConfig.DEBUG)
                        ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory
                        .createWithScheduler(Schedulers.io()))
                .build();

        statsService = retrofit.create(StatsService.class);
    }

    public StatsService getStatsService() {
        return statsService;
    }
}

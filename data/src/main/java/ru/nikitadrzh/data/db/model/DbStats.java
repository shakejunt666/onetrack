package ru.nikitadrzh.data.db.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DbStats extends RealmObject {
    @PrimaryKey
    private int sumCountOfSteps;

    public DbStats() {
    }

    public DbStats(int sumCountOfSteps) {
        this.sumCountOfSteps = sumCountOfSteps;
    }

    public int getSumCountOfSteps() {
        return sumCountOfSteps;
    }

    public void setSumCountOfSteps(int sumCountOfSteps) {
        this.sumCountOfSteps = sumCountOfSteps;
    }
}

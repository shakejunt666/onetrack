package ru.nikitadrzh.data.db;

import io.reactivex.Completable;
import io.reactivex.Single;
import ru.nikitadrzh.data.db.model.DbStats;

public interface StatsDao {
    Completable saveAchieveStats(int sumCountOfSteps);

    Single<DbStats> getAchieveStats();
}

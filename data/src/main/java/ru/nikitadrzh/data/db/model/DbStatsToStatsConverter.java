package ru.nikitadrzh.data.db.model;

import ru.nikitadrzh.domain.model.Stats;

public class DbStatsToStatsConverter {

    public Stats mapDbStatsToStats(DbStats dbStatsToMap) {

        return new Stats(0, 0, 0, dbStatsToMap.getSumCountOfSteps(),
                0, false);
    }
}

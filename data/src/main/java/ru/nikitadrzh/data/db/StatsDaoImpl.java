package ru.nikitadrzh.data.db;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.realm.Realm;
import ru.nikitadrzh.data.db.model.DbStats;
import ru.nikitadrzh.data.db.model.DbStatsToStatsConverter;

public class StatsDaoImpl implements StatsDao {

    private DbStatsToStatsConverter converter = new DbStatsToStatsConverter();

    @Override
    public Completable saveAchieveStats(int sumCountOfSteps) {
        try (Realm realm = Realm.getDefaultInstance()) {
            realm.executeTransaction(realm1 -> {
                realm1.delete(DbStats.class);
                realm1.copyToRealmOrUpdate(new DbStats(sumCountOfSteps));
            });
        }
        return Completable.complete();
    }

    @Override
    public Single<DbStats> getAchieveStats() {
        return Single.create(emitter -> {
            try (Realm realm = Realm.getDefaultInstance()) {
                realm.refresh();
                DbStats dbStats = realm.where(DbStats.class).findFirst();
                emitter.onSuccess(realm.copyFromRealm(dbStats));
            }
        });
//        return realm.where(DbStats.class).findFirstAsync()
//                .asFlowable()
//                .single(new DbStats(0))
//                .cast(DbStats.class)
//                .observeOn(Schedulers.io())
//                .map(dbStats -> converter.mapDbStatsToStats(dbStats));
    }
}

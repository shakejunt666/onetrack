package ru.nikitadrzh.data.model;

/**
 * POJO ApiModel
 */
public class ApiStats {
    public int aerobic;
    public long date;
    public int run;
    public int walk;
}


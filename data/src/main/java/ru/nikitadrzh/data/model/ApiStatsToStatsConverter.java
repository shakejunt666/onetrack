package ru.nikitadrzh.data.model;

import java.util.ArrayList;
import java.util.List;

import ru.nikitadrzh.domain.model.Stats;

public class ApiStatsToStatsConverter {

    public List<Stats> mapApiStatsToStats(List<ApiStats> apiStatsToMap) {
        List<Stats> stats = new ArrayList<>();
        for (ApiStats apiStats : apiStatsToMap) {
            stats.add(new Stats(apiStats.walk, apiStats.aerobic, apiStats.run, apiStats.date,
                    false));
        }
        return stats;
    }
}

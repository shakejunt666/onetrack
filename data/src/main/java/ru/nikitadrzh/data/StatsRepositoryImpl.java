package ru.nikitadrzh.data;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.nikitadrzh.data.db.StatsDao;
import ru.nikitadrzh.data.db.model.DbStatsToStatsConverter;
import ru.nikitadrzh.data.model.ApiStatsToStatsConverter;
import ru.nikitadrzh.data.service.StatsService;
import ru.nikitadrzh.domain.model.Stats;
import ru.nikitadrzh.domain.repository.StatsRepository;

public class StatsRepositoryImpl implements StatsRepository {

    private StatsService statsService;
    private ApiStatsToStatsConverter apiStatsToStatsConverter;

    private StatsDao statsDao;
    private DbStatsToStatsConverter dbStatsToStatsConverter;

    public StatsRepositoryImpl(StatsService statsService, ApiStatsToStatsConverter apiStatsToStatsConverter,
                               StatsDao statsDao, DbStatsToStatsConverter dbStatsToStatsConverter) {
        this.statsService = statsService;
        this.apiStatsToStatsConverter = apiStatsToStatsConverter;
        this.statsDao = statsDao;
        this.dbStatsToStatsConverter = dbStatsToStatsConverter;
    }

    /**
     * С сервера получаем ApiStats, конвертируем в Stats и пускаем дальше
     */
    @Override
    public Single<List<Stats>> getStats() {
        return statsService.getStats()
                .subscribeOn(Schedulers.io())
                .map(apiStatsToStatsConverter::mapApiStatsToStats)
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * из базы данных получаем Статистику Ожидаемую
     */
    @Override
    public Single<Stats> getAchievementStats() {
        return statsDao.getAchieveStats()
                .subscribeOn(Schedulers.io())
                .map(dbStats -> dbStatsToStatsConverter.mapDbStatsToStats(dbStats))
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Устанавливаем в базу данных Ожидаемую Статистику
     */
    @Override
    public Completable setExpectedSumCountOfSteps(int countOfSteps) {
        return statsDao.saveAchieveStats(countOfSteps)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}

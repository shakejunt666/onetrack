package ru.nikitadrzh.onetracktest;

import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import ru.nikitadrzh.onetracktest.dagger.ActivityComponent;
import ru.nikitadrzh.onetracktest.dagger.DaggerActivityComponent;
import ru.nikitadrzh.onetracktest.dagger.module.StatsPresenterModule;
import ru.nikitadrzh.onetracktest.model.StatsViewModel;
import ru.nikitadrzh.onetracktest.stats.StatsAdapter;
import ru.nikitadrzh.onetracktest.stats.StatsContract;

public class MainActivity extends AppCompatActivity implements StatsContract.View {
    private static ActivityComponent component;

    @Inject
    public StatsContract.Presenter presenter;

    @BindView(R.id.stats_recycler)
    RecyclerView statsRecyclerView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private StatsAdapter statsAdapter;
    private RecyclerView.LayoutManager statsLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // TODO: 01.08.2019 убать в даггер
        Realm.init(this);

        initDaggerComponent();
        component.inject(this);

        ButterKnife.bind(this);
        initRecyclerView();

        // TODO: 02.08.2019 вынести в билдер
        initAlertDialog();
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showStats(List<StatsViewModel> statsViewModels) {
        statsAdapter.onStatsUpdate(statsViewModels);
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showError(Throwable error) {

    }

    @Override
    public void setAchieveStats(int sumCountOfSteps) {
        statsAdapter.setAchieveCountOfSteps(sumCountOfSteps);
    }

    @Override
    public void showSetAchieveCompleted() {
        presenter.getStats();
    }

    private void initRecyclerView() {
        if (statsRecyclerView.getAdapter() == null) {
            statsAdapter = new StatsAdapter();
            statsRecyclerView.setAdapter(statsAdapter);
        } else {
            statsAdapter = (StatsAdapter) statsRecyclerView.getAdapter();
        }
        statsLayoutManager = new LinearLayoutManager(null);
        statsRecyclerView.setLayoutManager(statsLayoutManager);
    }

    // TODO: 31.07.2019   переместить в DaggerInitializer потом
    private void initDaggerComponent() {
        component = DaggerActivityComponent.builder()
                .statsPresenterModule(new StatsPresenterModule(this))
                .build();
    }

    private void initAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Set minimum steps:");
        final EditText sum = new EditText(this);
        sum.setInputType(InputType.TYPE_CLASS_NUMBER);
        builder.setView(sum);
        builder.setPositiveButton("OK", (dialogInterface, i) -> {
            if (sum.getText().toString().equals("")) {
                presenter.saveAchieveStats(0);
            } else {
                presenter.saveAchieveStats(Integer.parseInt(sum.getText().toString()));
            }
            progressBar.setVisibility(View.VISIBLE);
        });
        builder.setOnDismissListener(dialogInterface -> {
            presenter.getStats();
            presenter.getAchievedStats();
            progressBar.setVisibility(View.VISIBLE);
        });
        builder.show();
    }

    public static ActivityComponent getActivityComponent() {
        return component;
    }
}



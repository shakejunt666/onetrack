package ru.nikitadrzh.onetracktest.mapper;

import java.util.ArrayList;
import java.util.List;

import ru.nikitadrzh.domain.model.Stats;
import ru.nikitadrzh.onetracktest.model.StatsViewModel;

/**
 * Здесь прописывается логика, как Модель конвертируется во Вью модель
 */
public class StatsViewModelMapperImpl implements StatsViewModelMapper {

    @Override
    public List<StatsViewModel> mapStatsToViewModel(List<Stats> statsToMap) {
        List<StatsViewModel> statsViewModels = new ArrayList<>();
        for (Stats stat : statsToMap) {
            statsViewModels.add(new StatsViewModel(stat.getWalkSteps(), stat.getAerobicSteps(),
                    stat.getRunSteps(), stat.getDate(), stat.isAchieveGoals()));
        }
        return statsViewModels;
    }
}

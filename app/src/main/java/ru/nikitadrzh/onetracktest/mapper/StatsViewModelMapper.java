package ru.nikitadrzh.onetracktest.mapper;

import java.util.List;

import ru.nikitadrzh.domain.model.Stats;
import ru.nikitadrzh.onetracktest.model.StatsViewModel;

public interface StatsViewModelMapper {
    List<StatsViewModel> mapStatsToViewModel(List<Stats> statsToMap);
}

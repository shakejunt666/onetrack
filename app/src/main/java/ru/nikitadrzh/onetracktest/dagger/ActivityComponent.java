package ru.nikitadrzh.onetracktest.dagger;

import dagger.Component;
import ru.nikitadrzh.onetracktest.MainActivity;
import ru.nikitadrzh.onetracktest.dagger.module.DbModule;
import ru.nikitadrzh.onetracktest.dagger.module.ServiceModule;
import ru.nikitadrzh.onetracktest.dagger.module.StatsPresenterModule;
import ru.nikitadrzh.onetracktest.dagger.module.UseCaseModule;
import ru.nikitadrzh.onetracktest.stats.StatsPresenter;

@Component(modules = {StatsPresenterModule.class, ServiceModule.class, UseCaseModule.class,
        DbModule.class})
public interface ActivityComponent {
    void inject(MainActivity mainActivity);
    void inject(StatsPresenter statsPresenter);
}

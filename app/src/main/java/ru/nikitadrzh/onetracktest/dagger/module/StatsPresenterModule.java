package ru.nikitadrzh.onetracktest.dagger.module;

import dagger.Module;
import dagger.Provides;
import ru.nikitadrzh.onetracktest.MainActivity;
import ru.nikitadrzh.onetracktest.mapper.StatsViewModelMapper;
import ru.nikitadrzh.onetracktest.mapper.StatsViewModelMapperImpl;
import ru.nikitadrzh.onetracktest.stats.StatsContract;
import ru.nikitadrzh.onetracktest.stats.StatsPresenter;

@Module
public class StatsPresenterModule {

    public StatsContract.View view;

    public StatsPresenterModule(StatsContract.View view) {
        this.view = view;
    }

    @Provides
    public StatsContract.Presenter provideStatsPresenter() {
        StatsPresenter presenter = new StatsPresenter(view);
        MainActivity.getActivityComponent().inject(presenter);
        return presenter;
    }

    @Provides
    public StatsViewModelMapper providesStatsViewModelMapper() {
        return new StatsViewModelMapperImpl();
    }
}

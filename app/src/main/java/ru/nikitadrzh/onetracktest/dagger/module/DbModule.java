package ru.nikitadrzh.onetracktest.dagger.module;

import dagger.Module;
import dagger.Provides;
import ru.nikitadrzh.data.db.StatsDao;
import ru.nikitadrzh.data.db.StatsDaoImpl;
import ru.nikitadrzh.data.db.model.DbStatsToStatsConverter;

@Module
public class DbModule {

    @Provides
    public StatsDao provideStatsDao() {
        return new StatsDaoImpl();
    }

    @Provides
    public DbStatsToStatsConverter provideDbStatsToStatsConverter() {
        return new DbStatsToStatsConverter();
    }

}

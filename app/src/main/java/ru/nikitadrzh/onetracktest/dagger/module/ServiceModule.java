package ru.nikitadrzh.onetracktest.dagger.module;

import dagger.Module;
import dagger.Provides;
import ru.nikitadrzh.data.model.ApiStatsToStatsConverter;
import ru.nikitadrzh.data.service.StatsService;
import ru.nikitadrzh.data.service.StatsServiceImpl;

@Module
public class ServiceModule {

    @Provides
    public StatsService providesStatsService() {
        return new StatsServiceImpl();
    }

    @Provides
    public ApiStatsToStatsConverter providesApiStatsToStatsConverter() {
        return new ApiStatsToStatsConverter();
    }
}

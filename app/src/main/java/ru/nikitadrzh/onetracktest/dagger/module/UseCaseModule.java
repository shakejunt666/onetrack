package ru.nikitadrzh.onetracktest.dagger.module;

import dagger.Module;
import dagger.Provides;
import ru.nikitadrzh.data.StatsRepositoryImpl;
import ru.nikitadrzh.data.db.StatsDao;
import ru.nikitadrzh.data.db.model.DbStatsToStatsConverter;
import ru.nikitadrzh.data.model.ApiStatsToStatsConverter;
import ru.nikitadrzh.data.service.StatsService;
import ru.nikitadrzh.domain.interactor.GetAchievementStatsUseCase;
import ru.nikitadrzh.domain.interactor.SaveAchievementStatsUseCase;
import ru.nikitadrzh.domain.interactor.ShowStatsUseCase;
import ru.nikitadrzh.domain.repository.StatsRepository;

@Module
public class UseCaseModule {

    @Provides
    public StatsRepository providesStatsRepository(StatsService service, ApiStatsToStatsConverter converter,
                                                   StatsDao statsDao, DbStatsToStatsConverter dbStatsToStatsConverter) {
        return new StatsRepositoryImpl(service, converter, statsDao, dbStatsToStatsConverter);
    }

    @Provides
    public ShowStatsUseCase providesShowStatsUseCase(StatsRepository statsRepository,
                                                     GetAchievementStatsUseCase getAchievementStatsUseCase) {
        return new ShowStatsUseCase(statsRepository, getAchievementStatsUseCase);
    }

    @Provides
    public GetAchievementStatsUseCase providesGetAchievementStatsUseCase(StatsRepository statsRepository) {
        return new GetAchievementStatsUseCase(statsRepository);
    }

    @Provides
    public SaveAchievementStatsUseCase providesSaveAchievementStatsUseCase(StatsRepository statsRepository) {
        return new SaveAchievementStatsUseCase(statsRepository);
    }
}

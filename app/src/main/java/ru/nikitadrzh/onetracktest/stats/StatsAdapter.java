package ru.nikitadrzh.onetracktest.stats;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.nikitadrzh.onetracktest.R;
import ru.nikitadrzh.onetracktest.model.StatsViewModel;

/**
 * Адаптер для RecyclerView
 */
public class StatsAdapter extends RecyclerView.Adapter<StatsAdapter.StatsViewHolder> {
    private List<StatsViewModel> statsViewModels = new ArrayList<>();
    private int achieveCountOfSteps;

    @NonNull
    @Override
    public StatsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.stats_view, viewGroup, false);
        return new StatsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull StatsViewHolder statsViewHolder, int i) {
        statsViewHolder.setItem(statsViewModels.get(i), achieveCountOfSteps);
    }

    @Override
    public int getItemCount() {
        return statsViewModels.size();
    }

    public void onStatsUpdate(List<StatsViewModel> statsViewModels) {
        this.statsViewModels = statsViewModels;
        notifyDataSetChanged();
    }

    public int getAchieveCountOfSteps() {
        return achieveCountOfSteps;
    }

    public void setAchieveCountOfSteps(int achieveCountOfSteps) {
        this.achieveCountOfSteps = achieveCountOfSteps;
    }

    static final class StatsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.walk_view)
        TextView walk;

        @BindView(R.id.aerobic_view)
        TextView aerobic;

        @BindView(R.id.run_view)
        TextView run;

        @BindView(R.id.sum_view)
        TextView currentAchievedSumOfSteps;

        @BindView(R.id.date_view)
        TextView date;

        @BindView(R.id.goal_view)
        ImageView goalReached;

        @BindView(R.id.goal_text_view)
        TextView goalTextView;

        @BindView(R.id.walk_bar)
        TextView walkBar;

        @BindView(R.id.aerobic_bar)
        TextView aerobicBar;

        @BindView(R.id.run_bar)
        TextView runBar;

        @BindView(R.id.layout)
        LinearLayout layout;

        private StatsViewModel statsViewModel;

        public StatsViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setItem(StatsViewModel statsViewModel, int achieveCountOfSteps) {
            this.statsViewModel = statsViewModel;
            walk.setText(String.valueOf(statsViewModel.getWalkSteps()));
            walkBar.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    (float) statsViewModel.getWalkSteps() /
                            (float) statsViewModel.getSumCountOfSteps()));

            aerobic.setText(String.valueOf(statsViewModel.getAerobicSteps()));
            aerobicBar.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    (float) statsViewModel.getAerobicSteps() /
                            (float) statsViewModel.getSumCountOfSteps()));

            run.setText(String.valueOf(statsViewModel.getRunSteps()));
            runBar.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    (float) statsViewModel.getRunSteps() /
                            (float) statsViewModel.getSumCountOfSteps()));

            String itemDateStr = new SimpleDateFormat("dd.MM.yyyy")
                    .format(new Date(statsViewModel.getDate()));
            date.setText(itemDateStr);

            currentAchievedSumOfSteps.setText(String.valueOf(statsViewModel.getSumCountOfSteps()) +
                    " / " + String.valueOf(achieveCountOfSteps) + " steps");

            layout.setVisibility(View.INVISIBLE);
            if (statsViewModel.isAchieveGoals()) {
                layout.setVisibility(View.VISIBLE);
                goalReached.setImageResource(R.drawable.ic_star_rate);
                goalTextView.setText("Goal reached");
            }
        }
    }
}

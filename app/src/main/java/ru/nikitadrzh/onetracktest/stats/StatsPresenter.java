package ru.nikitadrzh.onetracktest.stats;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.nikitadrzh.domain.interactor.GetAchievementStatsUseCase;
import ru.nikitadrzh.domain.interactor.SaveAchievementStatsUseCase;
import ru.nikitadrzh.domain.interactor.ShowStatsUseCase;
import ru.nikitadrzh.onetracktest.mapper.StatsViewModelMapper;
import ru.nikitadrzh.onetracktest.model.StatsViewModel;

/**
 * Presenter, отображающий список объявлений
 */
public class StatsPresenter implements StatsContract.Presenter {
    private Disposable disposable;

    @Inject
    public ShowStatsUseCase showStatsUseCase;

    @Inject
    public SaveAchievementStatsUseCase saveAchievementStatsUseCase;

    @Inject
    public GetAchievementStatsUseCase getAchievementStatsUseCase;

    @Inject
    public StatsViewModelMapper statsViewModelMapper;

    private StatsContract.View view;

    public StatsPresenter(StatsContract.View view) {
        this.view = view;
    }

    @Override
    public void getStats() {
        disposable = showStatsUseCase.execute()
                .subscribeOn(Schedulers.io())
                .map(statsViewModelMapper::mapStatsToViewModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showStats, this::showError);
    }

    @Override
    public void saveAchieveStats(int sumCountOfSteps) {
        disposable = saveAchievementStatsUseCase.execute(sumCountOfSteps)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> showSetAchieveCompleted(sumCountOfSteps), this::showError);
    }

    @Override
    public void getAchievedStats() {
        disposable = getAchievementStatsUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(stats -> setAchievedStats(stats.getSumCountOfSteps()), this::showError);
    }

    private void setAchievedStats(int countOfSteps) {
        view.setAchieveStats(countOfSteps);
    }

    private void showStats(List<StatsViewModel> statsToShow) {
        view.showStats(statsToShow);
    }

    private void showError(Throwable error) {
        view.showError(error);
    }

    private void showSetAchieveCompleted(int sumCountOfSteps) {
        view.setAchieveStats(sumCountOfSteps);
        view.showSetAchieveCompleted();
    }
}


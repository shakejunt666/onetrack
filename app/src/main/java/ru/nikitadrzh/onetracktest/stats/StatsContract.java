package ru.nikitadrzh.onetracktest.stats;

import java.util.List;

import ru.nikitadrzh.onetracktest.model.StatsViewModel;

/**
 * Интерфейс описывающий работу презентера и вью, реализуется соотвественно ими
 */
public interface StatsContract {

    interface Presenter {
        void getStats();

        void saveAchieveStats(int sumCountOfSteps);

        void getAchievedStats();
    }

    interface View {
        void showStats(List<StatsViewModel> statsViewModels);

        void showError(Throwable error);

        void setAchieveStats(int sumCountOfSteps);

        void showSetAchieveCompleted();
    }
}

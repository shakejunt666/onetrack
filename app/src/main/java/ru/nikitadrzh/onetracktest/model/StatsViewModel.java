package ru.nikitadrzh.onetracktest.model;

public class StatsViewModel {
    private int walkSteps;
    private int aerobicSteps;
    private int runSteps;
    private int sumCountOfSteps;
    private long date;
    private boolean achieveGoals;

    public StatsViewModel(int walkSteps,
                          int aerobicSteps,
                          int runSteps,
                          long date,
                          boolean achieveGoals) {
        this.walkSteps = walkSteps;
        this.aerobicSteps = aerobicSteps;
        this.runSteps = runSteps;
        this.sumCountOfSteps = walkSteps + aerobicSteps + runSteps;
        this.date = date;
        this.achieveGoals = achieveGoals;
    }

    public int getWalkSteps() {
        return walkSteps;
    }

    public void setWalkSteps(int walkSteps) {
        this.walkSteps = walkSteps;
    }

    public int getAerobicSteps() {
        return aerobicSteps;
    }

    public void setAerobicSteps(int aerobicSteps) {
        this.aerobicSteps = aerobicSteps;
    }

    public int getRunSteps() {
        return runSteps;
    }

    public void setRunSteps(int runSteps) {
        this.runSteps = runSteps;
    }

    public int getSumCountOfSteps() {
        return sumCountOfSteps;
    }

    public void setSumCountOfSteps(int sumCountOfSteps) {
        this.sumCountOfSteps = sumCountOfSteps;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public boolean isAchieveGoals() {
        return achieveGoals;
    }

    public void setAchieveGoals(boolean achieveGoals) {
        this.achieveGoals = achieveGoals;
    }
}

package ru.nikitadrzh.domain.repository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import ru.nikitadrzh.domain.model.Stats;

/**
 * Интерфейс хранилища, который реализуется на внешних уровнях (Бд, интернет...)
 */
public interface StatsRepository {
    Single<List<Stats>> getStats();

    Single<Stats> getAchievementStats();

    Completable setExpectedSumCountOfSteps(int countOfSteps);
}

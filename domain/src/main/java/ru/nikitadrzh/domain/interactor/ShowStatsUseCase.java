package ru.nikitadrzh.domain.interactor;

import java.util.List;

import io.reactivex.Single;
import ru.nikitadrzh.domain.model.Stats;
import ru.nikitadrzh.domain.repository.StatsRepository;

/**
 * UseCase получения статистики
 */
public class ShowStatsUseCase {

    private final StatsRepository statsRepository;
    private final GetAchievementStatsUseCase getAchievementStatsUseCase;

    public ShowStatsUseCase(StatsRepository statsRepository,
                            GetAchievementStatsUseCase getAchievementStatsUseCase) {
        this.statsRepository = statsRepository;
        this.getAchievementStatsUseCase = getAchievementStatsUseCase;
    }

    public Single<List<Stats>> execute() {
        return statsRepository.getStats()
                .zipWith(getAchievementStatsUseCase.execute(), (stats, achievementStatistic) -> {
                    for (Stats statistic : stats) {
                        if (statistic.getSumCountOfSteps() >=
                                achievementStatistic.getSumCountOfSteps()) {
                            statistic.setAchieveGoals(true);
                        }
                    }
                    return stats;
                });
    }
}

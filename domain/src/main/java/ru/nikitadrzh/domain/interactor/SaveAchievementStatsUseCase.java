package ru.nikitadrzh.domain.interactor;

import io.reactivex.Completable;
import ru.nikitadrzh.domain.repository.StatsRepository;

public class SaveAchievementStatsUseCase {
    private final StatsRepository statsRepository;

    public SaveAchievementStatsUseCase(StatsRepository statsRepository) {
        this.statsRepository = statsRepository;
    }

    public Completable execute(int sumCountOfSteps) {
        return statsRepository.setExpectedSumCountOfSteps(sumCountOfSteps);
    }
}

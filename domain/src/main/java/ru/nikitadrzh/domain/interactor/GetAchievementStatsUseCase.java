package ru.nikitadrzh.domain.interactor;

import java.util.List;

import io.reactivex.Single;
import ru.nikitadrzh.domain.model.Stats;
import ru.nikitadrzh.domain.repository.StatsRepository;

public class GetAchievementStatsUseCase {
    private final StatsRepository statsRepository;

    public GetAchievementStatsUseCase(StatsRepository statsRepository) {
        this.statsRepository = statsRepository;
    }

    public Single<Stats> execute() {
        return statsRepository.getAchievementStats();
    }
}
